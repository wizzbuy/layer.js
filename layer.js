(function (document, $) {
  'use strict';

  // LAYER CLASS DEFINITION
  // ======================
  var Layer = function (element, options) {
    this.options        = options;
    this.$body          = $(document.body);
    this.$element       = $(element);
    this.$backdrop      = null;
    this.isShown        = null;
  };

  Layer.DEFAULTS = {
    backdrop: true,
    closeButton: true
  };

  Layer.prototype.show = function () {
    var self = this;
    var e = $.Event('show.layer');

    if (self.$element.is('script')) {
      self.$element = $(self.$element.html());
    }

    this.$element.trigger(e);

    if (this.isShown || e.isDefaultPrevented()) {
      return;
    }

    this.isShown = true;

    if (this.options.backdrop) {
      this.$body.addClass('layer-open');
    }

    this.$element.on('click.dismiss.layer', '[data-dismiss="layer"]',
        $.proxy(this.hide, this));

    if (this.options.callback) {
      this.$element.on('hidden.layer', function (e, args) {
        self.options.callback(e, args);
      });
    }

    this.backdrop(function () {
      self.$element.addClass('layer-content');
      self.$element.appendTo(self.$body);

      if (self.options.backdrop && self.options.closeButton) {
        $('<i data-dismiss="layer" data-dismiss-args="close" />')
          .addClass('fa')
          .addClass('fa-times')
          .addClass('close-layer')
          .prependTo(self.$element);
      }

      self.$element.show();

      var e = $.Event('shown.layer');

      self.$element
        .trigger('focus')
        .trigger(e);
    });
  };

  Layer.prototype.hide = function (e) {
    var args = null;

    if (e) {
      e.preventDefault();

      args = $(e.currentTarget).data('dismiss-args');
    }

    e = $.Event('hide.layer');

    this.$element.trigger(e);

    if (!this.isShown || e.isDefaultPrevented()) {
      return;
    }

    this.isShown = false;

    this.$body.removeClass('layer-open');
    this.$element.off('click.dismiss.layer');

    this.hideLayer(args);
  };

  Layer.prototype.hideLayer = function (args) {
    var self = this;

    this.$element.hide();
    this.$element.removeClass('layer-content');
    this.$element.find('.close-layer').remove();

    this.backdrop(function () {
      self.$element.trigger('hidden.layer', args);
    });
  };

  Layer.prototype.removeBackdrop = function () {
    if (this.$backdrop) {
      this.$backdrop.remove();
    }

    this.$backdrop = null;
  };

  Layer.prototype.backdrop = function (callback) {
    if (this.isShown && this.options.backdrop) {
      this.$backdrop = $('<div class="layer-backdrop" />').appendTo(this.$body);

      this.$element.on('click.dismiss.layer', $.proxy(function (e) {
        if (e.target !== e.currentTarget) {
          return;
        }

        // XXX This isn't doing anything!
      }, this));
    }
    else if (!this.isShown && this.options.backdrop) {
      this.removeBackdrop();
    }

    if (typeof callback === 'function') {
      callback();
    }
  };


  // LAYER PLUGIN DEFINITION
  // =======================
  function Plugin (option, callback) {
    return this.each(function () {
      var $this   = $(this);
      var data    = $this.data('layer');
      var options = $.extend(
        {}, Layer.DEFAULTS, $this.data(), typeof option === 'object' && option
      );

      if (typeof callback === 'function') {
        options.callback = callback;
      }

      if (!data) {
        $this.data('layer', (data = new Layer(this, options)));
      }

      if (typeof option === 'string') {
        data[option]();
      }
      else if (options.show) {
        data.show();
      }
    });
  }

  var old = $.fn.layer;

  $.fn.layer             = Plugin;
  $.fn.layer.Constructor = Layer;


  // LAYER NO CONFLICT
  // =================
  $.fn.layer.noConflict = function () {
    $.fn.layer = old;
    return this;
  };

})(document, jQuery);
